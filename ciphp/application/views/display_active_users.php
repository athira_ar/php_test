<!DOCTYPE html>
<html>
<head>
<title>Display Records</title>
<style>
    /* Style the header */
    .header {
      background-color: #F1F1F1;
      text-align: center;
      padding: 20px;
    }
    /* Style the top navigation bar */
    .topnav {
      overflow: hidden;
      background-color: #333;
    }

    /* Style the topnav links */
    .topnav a {
      float: left;
      display: block;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }

    /* Change color on hover */
    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }
    .column {
      /*float: left;*/
      width: 33.33%;
      padding: 15px;
    }
</style>
</head>

<body>

    <center>
        <table width="600" border="1" cellspacing="5" cellpadding="5">
            <tr style="background:#CCC">
                <th>id</th>
                <th>email</th>
                <th>status</th>
                
                <th>Change Status</th>
            </tr>
            <?php
                $i=1;
                foreach($data as $row)
                {
                    echo "<tr>";
                    echo "<td>".$row->id."</td>";
                    echo "<td>".$row->email."</td>";
                    echo "<td>".$row->status."</td>";
                    $statusButton=$row->status=='active'?'Deactivate':'Activate';
                    echo "<td><a href='changestatus?id=".$row->id."&email=".$row->email."&password=".$row->password."'>".$statusButton."</a></td>";
                    echo "</tr>";
                    $i++;
                }
            ?>
        </table>
    </center>   
</body>
</html>
