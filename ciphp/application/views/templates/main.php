<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">
            #header {
                width:100%;
                height:100px;
                background:#ff0;
            }
            #footer {
                width:100%;
                height:100px;
                background:#f00;
            }
            #sidebar {
                width:30%;
                height:600px;
                background:#f00;
                float:left;
            }
            #content {
                width:30%;
                height:600px;
                float:left;
            }
            /* Style the header */
    .header {
      background-color: #F1F1F1;
      text-align: center;
      padding: 20px;
    }
    
    .footer {
      position: fixed;;
      left: 0;
      bottom: 0;
      z-index: 10;
  /* position: absolute; */
      width: 100%;
      height:50px;
      overflow: hidden;
      background-color: #000000;
      /* background-color: rgb(0, 9, 12); */
      color: white;
      text-align: center;
      font-family: Ubuntu-Regular, sans-serif;
    }
    /* Style the top navigation bar */
    .topnav {
      overflow: hidden;
      background-color: #000000;
    }

    /* Style the topnav links */
    .topnav a {
      float: left;
      display: block;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
    }

    /* Change color on hover */
    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }
    .column {
        /*float: left;*/
        width: 33.33%;
        padding: 15px;
    }
    .clear {clear:both}
	</style>
</head>
<body>
<?php if ($header) {
    echo $header;
//    if ($userName) {
//        echo $userName;
//    }
} if ($sidebar) {
    echo $sidebar;
}?>
<div id="content" <?php if ($page) {
    echo $page;
    }?> </div>
    <div class='clear'></div>
        <?php if ($footer) {
    echo $footer;
}?>

</body>
</html>