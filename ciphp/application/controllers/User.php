<?php
class User extends CI_Controller 
{
	public function __construct()
	{
	parent::__construct();
	$this->load->database();
	$this->load->helper('url', 'form');
        $this->load->library('session','form_validation');
        $this->load->model('Admin_Model');
        
	}

	public function index()
	{
		$this->load->helper(array('form', 'url'));

                $this->load->library('form_validation');
                $this->form_validation->set_rules('name', 'Username', 'required');
                $this->form_validation->set_rules('pass', 'Password', 'required');
                $this->form_validation->set_rules('confirm', 'Password Confirmation', 'required|matches[pass]');
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if($this->input->post('register'))
		{
		$n=$this->input->post('name');
		$e=$this->input->post('email');
		$p=$this->input->post('pass');
                $cp=$this->input->post('confirm');
		$c=$this->input->post('address');
		
		$que=$this->db->query("select * from users where email='".$e."'");
		$row = $que->num_rows();
                 $unique =0;
		if($row)
		{
		$unique =1;
                }
		else
		{
		
		}
                
                
                if ($this->form_validation->run() == FALSE )
                {
                    $data['error']="<h3 style='color:red'>Validation Errors!!</h3>";
                }else if($unique ==1){
                    $data['error']="<h3 style='color:red'>This user already exists</h3>";
                }
                else
                {
                       $que=$this->db->query("insert into users values('','$n','$e','$p','$c','')");
		
                       $data['error']="<h3 style='color:blue'>Your account created successfully</h3>";
                }
			
		}
                 $this->load->view('user_registration',@$data);	
	
	}
        public function login()
	{
		
		if($this->input->post('login'))
		{
			$e=$this->input->post('email');
			$p=$this->input->post('pass');
	
			$que=$this->db->query("select * from login where email='".$e."' and password='$p' and status='active' ");
			$row = $que->num_rows();
			$result= $que->result();
                        
                if($result[0]->role=='admin') {
                    $this->session->set_flashdata('email',$result[0]->email);
                    redirect("Admin/dispregusers");
                }else if($result[0]->role=='user'){
                    $this->session->set_flashdata('email',$result[0]->email);
                    redirect("User/dispuserprofile/");
                } else
			{
		$data['error']="<h3 style='color:red'>Invalid login details or admin need to active your account. please contact admin</h3>";
			}	
		}
		$this->load->view('login',@$data);		
	}
	public function dispuserprofile()
	{$email=$this->session->flashdata('email');
	$result['data']=$this->Admin_Model->selectUserDetails($email);
         $this->page='display_user_details';
//       
        $this->load->view($this->page,$result);
	}
        
        
        public function uploadImg($id) 
	{
            $config['upload_path'] = './upload/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2000;
            $config['max_width'] = 1500;
            $config['max_height'] = 1500;

            $this->load->library('upload', $config);
             $this->load->model('Admin_Model');
            if (!$this->upload->do_upload('photo')) 
                    {
                $error = array('error' => $this->upload->display_errors());

            } 
                    else 
                    {
                $data = array('image_metadata' => $this->upload->data());
                $img = $this->upload->data();
                $imgdata = file_get_contents($img['full_path']);
                $this->Admin_Model->update_dp($id, $imgdata);// pass image name

        }
        
                    }
        
        public function editdetails()
	{
	$id=$this->input->get('id');
        

        $this->page='edit_user_details';
        $result['data'][0]->id=$this->input->get('id');
        $result['data'][0]->name=$this->input->get('name');
        $result['data'][0]->address=$this->input->get('address');
        $result['data'][0]->email=$this->input->get('email');
        $result['data'][0]->password=$this->input->get('password');
        $this->load->view($this->page,$result);
	
	if($this->input->post('pic'))
		{
		$this->uploadImg($id);
		}
	
		elseif($this->input->post('edit'))
		{
               
		$user_name=$this->input->post('name');
		$user_password=$this->input->post('password');
                $user_address=$this->input->post('address');
                $this->session->set_flashdata('email',$result['data'][0]->email);

		$this->Admin_Model->editUserDetails($id,$user_name,$user_password,$user_address);
		redirect("User/dispuserprofile/");
		}
	}
	function dashboard()
	{
	$this->load->view('dashboard');
	}
        
}
?>
