<?php
class Admin extends CI_Controller 
{
        protected $data = array();
        private $mydata = array();
	public function __construct()
	{
	//call CodeIgniter's default Constructor
            
	parent::__construct();
	
        //load database libray manually
	$this->load->database();
	$this->load->helper('url');
	//load Model
	$this->load->model('Admin_Model');
        
        $this->layout();
	}
public function layout()
	{
    $this->template['header']=$this->load->view('templates/header',$this->data,TRUE);
    $this->template['footer']=$this->load->view('templates/footer',$this->data,TRUE);
    $this->template['sidebar']=$this->load->view('templates/navigation',$this->data,TRUE);
}

        public function dispregusers()
	{
	$result['data']=$this->Admin_Model->selectRegisteredUsers();
        $this->page='display_registered_users';
        $this->template['page']=$this->load->view($this->page,$result,TRUE);
        $this->load->view('templates/main',$this->template);
	}
        public function dispactusers()
	{
	$result['data']=$this->Admin_Model->selectActiveUsers();
        $this->page='display_active_users';
        $this->template['page']=$this->load->view($this->page,$result,TRUE);
        $this->load->view('templates/main',$this->template);
	}
        public function giveaccess()
	{
	$id=$this->input->get('id');
        $email=$this->input->get('email');
        $password=$this->input->get('password');
	
	
	
		$this->Admin_Model->giveaccess($id,$email,$password,'user','active');
		redirect("Admin/dispregusers");
		
	}
        public function changestatus(){
            $id=$this->input->get('id');
            $this->Admin_Model->changestatus($id);
		redirect("Admin/dispactusers");
        }

}
